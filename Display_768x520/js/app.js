var App = function () {

	var data = [{
		img: "images/bg1_768x520.jpg",
		txt: "images/copy1_768x520.png",
		url: "http://rewrite.ca.com/appecon"
	},{
		img: "images/bg2_768x520.jpg",
		txt: "images/copy2_768x520.png",
		url: "http://rewrite.ca.com/appecon"
	},{
		img: "images/bg3_768x520.jpg",
		txt: "images/copy3_768x520.png",
		url: "http://rewrite.ca.com/appecon"
	},{
		img: "images/bg4_768x520.jpg",
		txt: "images/copy4_768x520.png",
		url: "http://rewrite.ca.com/appecon"
	}];

	var milliseconds = {
		betweenSlides   : 4000,
		transition      : 1000,
		cycleBuffer     : 100,
		loadUnderDuress : 600,
		throttle		: 2000,
		scrollTimeout	: 1500
	};

	var pixels = {
		stdWidth: 768,
		maxWidth: 1300,
		height:   520,
		minDrag:  4
	};

	var interval = null,
		$slides = [],
		trackStart = null,
		swipeDirection = null,
		$newSwipeSlide = null;

	var init = function() {
		$slides = $.map(data, appendSlide);
		$slides[0].addClass('active');
		loadSlideContent($slides[0]);
		loadControls();
		setControls(0);
		resetScroll();
		setTimeout(function() {
			loadSlideContent($slides[1]);
		}, milliseconds.throttle);
	};

	var inited = function() {
		return $slides.length > 0;
	};

	var isIE = function() {
		return window.navigator.userAgent.indexOf("MSIE ") > 0;
	};

	var isIE9 = function() {
		return $('html').hasClass('ie9');
	};

	var isTouchDevice = function() {
		return $('html').hasClass('touch-device');
	};

	var appendSlide = function(def, index) {
		var $slide = $('<div />', {
			'class'      : 'slide', 
			'href'       : def.url,
			'target'     : '_blank',
			'data-index' : index,
			'html'       : '<div class="wrapper"><div class="logo" /></div>'
		});
		$('.slider').append($slide);
		return $slide;
	};

	var slideAfter = function($slide) {
		return $slides[$slide.data('index') + 1] || $slides[0];
	};

	var slideBefore = function($slide) {
		return $slides[$slide.data('index') - 1] || $slides[$slides.length - 1];
	};

	var loadSlideContent = function($slide) {
		var def;
		if ($slide.data('loaded')) { return true; }
		def = data[$slide.data('index')];
		$slide.css('background-image', "url('" + def.img + "')");
		$slide.find('.wrapper').append($('<img src="' + def.txt + '" />'));
		$slide.data('loaded', true);
		return false;
	};

	var loadControls = function() {
		var i, 
			$controls = $('.controls');
		for (i = 0; i < data.length; i++) {
			$controls.append('<div class="dot"/>');
		}
	};

	var resetScroll = function() {
		$(window).scrollLeft($(window).width());
	};

	var setControls = function(index) {
		var dots = $('.controls .dot');
		dots.removeClass('active');
		dots.eq(index).addClass('active');
	};

	var bodyClass = function(cls, setVal) {
		var retValue;
		if (arguments.length === 0) {
			retValue = $('body').hasClass(cls);
		} else {
			retValue = !!setVal;
			$('body').toggleClass(cls, retValue);
		}
		return retValue;
	};

	var animating = function(setVal) {
		return bodyClass('animating', setVal);
	};

	var swiping = function(setVal) {
		return bodyClass('swiping', setVal);
	}

	var setSlideState = function($slide, state) {
		$slide.removeClass('left active right').addClass(state);
	};

	var animateSlide = function($slide, state, duration, easing) {
		var leftProp;
		switch(state) {
	    case 'left':
	        leftProp = '-100%';
	        break;
	    case 'active':
	        leftProp = 0;
	        break;
	    default:
	        leftProp = '100%';
		}
		$slide.animate(
			{ left: leftProp }, 
			duration || milliseconds.transition, 
			easing || 'swing',
			function() {
				setSlideState($slide, state);
				$slide.css({ left: "" });
			}
		);
	};

	var beginSwipe = function(direction) {
		var $activeSlide = $('.active.slide');
			swipeDirection = direction;
		if (!animating() && !swiping()) {
			swiping(true);
			if (direction === 'right') {
				$newSwipeSlide = slideBefore($activeSlide);
				setSlideState($newSwipeSlide, 'left');
			} else {
				$newSwipeSlide = slideAfter($activeSlide);
				setSlideState($newSwipeSlide, 'right');
			}
			loadSlideContent($newSwipeSlide);
		}
	};

	var swipeDrag = function(xOffset) {
		$('.active.slide').css('left', xOffset);
		if (swipeDirection === 'right') {
			$newSwipeSlide.css('left', xOffset - $newSwipeSlide.width());
		} else {
			$newSwipeSlide.css('left', xOffset + $newSwipeSlide.width());			
		}
	};

	var completeSwipe = function($activeSlide, $nextSlide, state) {
		var duration = milliseconds.transition / 2;
		animateSlide($activeSlide, state, duration, 'easeOutQuad');
		animateSlide($nextSlide, 'active', duration, 'easeOutQuad');
		setControls($nextSlide.data('index'));
		setTimeout(function() {
			swiping(false);
		}, duration);
	};

	var swapSlides = function($activeSlide, $nextSlide, direction) {
		var bufferTime = milliseconds.cycleBuffer,
			$afterNextSlide = $slides;
		if(!animating() && !swiping()) { 
			setSlideState($nextSlide, (direction === 'left') ? 'right' : 'left');
			if(!loadSlideContent($nextSlide)) { 
				bufferTime = milliseconds.loadUnderDuress; // if not yet preloaded
			}
			setTimeout(function() {
				animating(true);
				if(isIE9()) {
					animateSlide($activeSlide, direction);
					animateSlide($nextSlide, 'active');
				} else {
					setSlideState($activeSlide, direction);
					setSlideState($nextSlide, 'active');
				}
				setControls($nextSlide.data('index'));
				setTimeout(function() {
					animating(false);
					loadSlideContent(slideAfter($nextSlide));
				}, milliseconds.transition);
			}, bufferTime);
		}
	};

	var advanceSlide = function() {
		var $activeSlide = $('.slide.active');
		swapSlides($activeSlide, slideAfter($activeSlide), 'left');
	};

	var jumpToSlide = function(index) {
		var $activeSlide = $('.slide.active'),
			activeIndex = $activeSlide.data('index'),
			$nextSlide = $slides[index],
			direction = (index > activeIndex) ? 'left' : 'right';
		if (index !== activeIndex) {
			stopInterval();
			swapSlides($activeSlide, $nextSlide, direction);
			startInterval();
		}
	};

	var stopInterval = function() {
		if(interval) {
			clearInterval(interval);
		}
	};

	var startInterval = function() {
		stopInterval();
		interval = setInterval(advanceSlide, milliseconds.betweenSlides);
	};

	var beginTracking = function(x) {
		stopInterval();
		trackStart = {x: x};
		swipeDirection = null;
	};

	var followMouse = function(x) {
		if (trackStart) {
			if (!swipeDirection) {
				if (x >= trackStart.x + pixels.minDrag) {
					beginSwipe('right');
				} else if (x <= trackStart.x - pixels.minDrag) {
					beginSwipe('left');
				}
			}
			if (swipeDirection) {
				swipeDrag(x - trackStart.x);
			}
		}
	};

	var finishTracking = function(x) {
		var $activeSlide = $('.active.slide');
		if (swipeDirection === 'right') {
			if (x > trackStart.x + pixels.minDrag) {
				completeSwipe($activeSlide, $newSwipeSlide, 'right');
			} else {
				completeSwipe($newSwipeSlide, $activeSlide, 'left');				
			}
		} else if (swipeDirection === 'left') {
			if (x < trackStart.x - pixels.minDrag) {
				completeSwipe($activeSlide, $newSwipeSlide, 'left');
			} else {
				completeSwipe($newSwipeSlide, $activeSlide, 'right');				
			}
		} else {
			window.open(data[$activeSlide.data('index')].url,'_blank');
		}

		trackStart = null;
		swipeDirection = null;
		startInterval();
	};

	var bind = function() {

		var $dots = $('.controls .dot'),
			startScroll = 'loading';

		$dots.on('click', function(e) {
			e.stopPropagation();
			jumpToSlide($dots.index($(this)));
		}).on('mousedown mousemove mouseup', function(e) {
			e.stopPropagation();
		});

		$('body').on('mousedown', function(e) {
			if (isIE() || isTouchDevice()) { return; }
			beginTracking(e.pageX);
		}).on('mousemove', function(e) {
			if (isIE() || isTouchDevice()) { return; }
			followMouse(e.pageX);
		}).on('mouseup', function(e) {
			if (isIE() || isTouchDevice()) { return; }
			finishTracking(e.pageX);
		});

		$('body').on('touchstart', function() {
			$('html').addClass('touch-device');
		})

		// $(window).on('scroll', function(e) {
		// 	var scrollLeft = $(this).scrollLeft();
		// 	if (isIE9() || isTouchDevice()) { return; }
		// 	if (trackStart) {
		// 		followMouse(scrollLeft);
		// 		finishTracking(scrollLeft);
		// 		resetScroll();
		// 	} else if (startScroll !== 'loading' && 
		// 			   (!startScroll ||
		// 			    ((new Date()).getTime() - startScroll > milliseconds.scrollTimeout))) {
		// 		beginTracking(scrollLeft);
		// 		startScroll = (new Date()).getTime();
		// 	}
		// }).on('scrollstart', function() {
		// 	console.log('scrollstart');
		// });

		// setTimeout(function() {
		// 	startScroll = null;
		// }, milliseconds.cycleBuffer);

	};

	var unbind = function() {
		$('.controls .dot').off('click');
		$('body').off('mousedown mouseover mousemove touchstart');
	};

	this.on = function () {
		if (!inited()) { init(); }
		startInterval();
		bind();
	};

	this.off = function () {
		stopInterval();
		unbind();
	};
};

$(document).ready(function () { 
	var app = new App(); 
	app.on();
	document.addEventListener("visibilitychange", function() {
		if(document.visibilityState === 'visible') {
			app.on();
		} else {
			app.off();
		}
	});
});